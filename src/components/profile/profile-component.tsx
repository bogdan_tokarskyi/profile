import * as React from 'react';
import {useState} from 'react';
import {numberFormatter} from './helpers';

import './profile-component.scss';

export type starRatings = 0 | 1 | 2 | 3 | 4 | 5;

export type ProfileComponentProps = {
    name: string,
    imgUrl: string,
    city: string,
    title: string,
    country: string,
    profileViews: number,
    websiteViews: number,
    averageRating: starRatings,
}

function Stars({stars}) {
    const MAX_STARS = 5;
    const outputStart = [];

    for (let i = 0; i < MAX_STARS; i++) {
        outputStart.push(
            <i key={i} className={`profile__icon profile__star ${i < stars ? 'profile__star_active' : ''}`}/>
        )
    }

    return <div className="profile__stars-holder">{outputStart}</div>;
}

function Profile(props: ProfileComponentProps) {

    const [liked, switchLike] = useState(false);

    return (
        <div className="profile">
            <div className="profile__header">
                <h2 className="profile__title">Developer Profile</h2>
                <i
                    className={`profile__icon profile__like ${liked ? 'profile__like_active' : ''}`}
                    onClick={() => switchLike(!liked)}
                />
            </div>
            <div className="profile__body">
                <div className="profile__picture" style={{backgroundImage: `url("${props.imgUrl}")`}}/>
                <h2 className="profile__name overflow-ellipsis">{props.name}</h2>
                <div className="profile__sub-title overflow-ellipsis">{props.title}</div>
                <div className="profile__sub-title overflow-ellipsis">{props.city}, {props.country}</div>
            </div>
            <div className="profile__footer d-flex">
                <div className="profile__footer-section">
                    <span className="profile__number">{numberFormatter(props.profileViews)}</span>
                    <span className="profile__capture">Profile Views</span>
                </div>
                <div className="profile__footer-section">
                    <span className="profile__number">{numberFormatter(props.websiteViews)}</span>
                    <span className="profile__capture">Website Views</span>
                </div>
                <div className="profile__footer-section">
                    <Stars stars={props.averageRating}/>
                    <span className="profile__capture">Avg. Rating</span>
                </div>
            </div>
        </div>
    );
}

export default Profile;