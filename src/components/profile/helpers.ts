export function numberFormatter(inputNumber: number) {
    const LIMIT = 999999;

    let assignM = false;
    let outputNumber;

    if (inputNumber > LIMIT) {
        assignM = true;
        outputNumber = `${Math.floor(inputNumber / LIMIT + 1)}`;
    } else {
        outputNumber = `${inputNumber}`
    }

    if (outputNumber.length > 3) {
        outputNumber = outputNumber.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return `${outputNumber}${assignM ? 'M' : ''}`
}