import * as React from 'react';
import {mount} from 'enzyme/build';
import Profile from '../profile-component';
import {ProfileData} from '../../App';

const getMounted = () => mount(
    <Profile {...ProfileData}/>
);

describe('Profile', () => {

    const wrapper = getMounted();
    const likeButton = wrapper.find('.profile__like');

    it('should make `Like button` active', () => {
        expect(wrapper.find('.profile__like_active').length).toBe(0);
        likeButton.simulate('click');
        expect(wrapper.find('.profile__like_active').length).toBe(1);
    });

    it('should display `Stars` correctly', () => {
        expect(wrapper.find('.profile__star').length).toBe(5);
        expect(wrapper.find('.profile__star_active').length).toBe(4);
    });

});
