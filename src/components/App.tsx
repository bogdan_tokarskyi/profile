import * as React from 'react';
import {hot} from 'react-hot-loader';
import Profile, {starRatings} from './profile/profile-component';

const averageRating: starRatings = 4;

export const ProfileData = {
    name: 'Bohdan Tokarskyi',
    country: 'Ukraine',
    city: 'Nechaivka',
    profileViews: 5000,
    websiteViews: 1234,
    averageRating,
    imgUrl: 'https://www.upwork.com/profile-portraits/c1Upsk115_llwgIvH93vat9-6Uj_x1-09ruTAg9kiyJoN3sdLhllyugPfNbQQoG14G',
    title: 'Frontend Engineer'
};

class App extends React.Component<{}, undefined> {
    public render() {
        return (
            <Profile {...ProfileData} />
        );
    }
}

declare let module: object;

export default hot(module)(App);
